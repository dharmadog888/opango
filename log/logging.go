// pangolinlog offers basic application level logging services
// to the
package log

import (
	"fmt"
	"os"
	"strings"

	"github.com/op/go-logging"
)

const (
	DEBUG    = iota
	INFO     = iota
	NOTICE   = iota
	WARNING  = iota
	ERROR    = iota
	CRITICAL = iota
	FATAL    = iota
)

// following the pango module pattern
type logger struct {
	logger      logging.Logger
	loglevel    int
	initialized bool
}

// formatted messaging, needs work
func LogMsgf(level string, fstr string, args ...interface{}) {
	LogMsg(level, fmt.Sprintf(fstr, args...))
}

// standard messaging
func LogMsg(level string, msg string) {

	if !plog.initialized {
		plog.setup()
		plog.logger.Info("Logging Configured")
	}

	fn := plog.logger.Notice
	lvl := NOTICE

	switch strings.ToUpper(level) {
	case "DEBUG":
		fn = plog.logger.Debug
		lvl = DEBUG
	case "INFO":
		fn = plog.logger.Info
		lvl = INFO
	case "WARNING":
		fn = plog.logger.Warning
	case "ERROR":
		fn = plog.logger.Error
		lvl = ERROR
	case "CRITICAL":
		fn = plog.logger.Critical
		lvl = CRITICAL
	case "FATAL":
		panic(msg)
	}

	if lvl >= plog.loglevel {
		fn(msg)
	}
}

// configure logging for 2 channels. The first
// will output debug level info to stdout and
// the second will output warning level info to
// stderr
func (pl *logger) setup() {

	if !pl.initialized {
		// create the logger mechanism
		pl.logger = *logging.MustGetLogger("PangoLog")

		// create two backends for for os.Stdout and os.Stderr.
		backend1 := logging.NewLogBackend(os.Stdout, "", 0)
		backend2 := logging.NewLogBackend(os.Stderr, "", 0)

		// err level formatting
		format := logging.MustStringFormatter(
			`%{color}[%{time:15:04:05.000}] %{level:s} %{color:reset} %{message}`,
		)

		// add function name to warning level stuff and std for console log
		backend1Formatter := logging.NewBackendFormatter(backend1, format)
		backend2Formatter := logging.NewBackendFormatter(backend2, format)

		// Only errors and more severe messages should be sent to backend1
		backend1Leveled := logging.AddModuleLevel(backend1Formatter)
		backend1Leveled.SetLevel(logging.DEBUG, "")

		backend2Leveled := logging.AddModuleLevel(backend2Formatter)
		backend2Leveled.SetLevel(logging.WARNING, "")

		// Set the backends to be used.
		logging.SetBackend(backend1Leveled, backend2Leveled)

		pl.initialized = true
	}
}

// set log level threshold for en/disabling output
func SetLogLevel(lvl int) int {
	if !plog.initialized {
		plog.setup()
		plog.logger.Info("Logging Configured")
	}
	rv := plog.loglevel
	plog.loglevel = lvl

	plog.logger.Notice("Logging Level set", lvl)

	return rv
}

// logger variable
var plog logger
