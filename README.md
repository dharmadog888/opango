# Opango - Open Pangolin#

Opango is the open source branch of the Pangolin project. We
use this project to expose some nifty things that think might
be useful for the Go gang in general. The prime motivators for
the opening are contained in the httpd package and consists of
a REST based web routing / controller system that plugs into
the golang net/http package to implement RESTful APIs.

### Project Info ###

* Open source tools for Go Web Dev
* Version 0.1.2
* GOLANG

### Setup ###

* get opango
* Tour the 4x package for sample server


### The buck stops here! ###

* David Connell <dharmadog888@gmail.com>

### TODO: ###
* finish README ;)
